// Validación de formulario de perfil de usuario
document.getElementById('profile-form').addEventListener('submit', function (e) {
    e.preventDefault();
  
    const username = document.getElementById('username').value;
    const email = document.getElementById('email').value;
  
    if (username && email) {
      alert('Perfil de usuario guardado con éxito.');
    } else {
      alert('Por favor, complete todos los campos.');
    }
  });
  
  // Validación de formulario de configuración de usuario
  document.getElementById('settings-form').addEventListener('submit', function (e) {
    e.preventDefault();
  
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;
  
    if (password && confirmPassword) {
      if (password === confirmPassword) {
        alert('Configuración de usuario guardada con éxito.');
      } else {
        alert('Las contraseñas no coinciden. Por favor, inténtelo de nuevo.');
      }
    } else {
      alert('Por favor, complete todos los campos.');
    }
  });
  