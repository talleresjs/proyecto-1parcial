
document.getElementById('syllabus-edit-form').addEventListener('submit', function (e) {
    e.preventDefault();
  
    const department = document.getElementById('department').value;
    const course = document.getElementById('course').value;
    const contents = document.getElementById('contents').files[0];
  
    if (course && department && contents) {
      // Verificar si el archivo es un PDF
      if (contents.type === 'application/pdf') {
        alert('Sílabo guardado con éxito.');
        document.getElementById('syllabus-edit-form').reset();
      } else {
        alert('Por favor, seleccione un archivo PDF.');
      }
    } else {
      alert('Por favor, complete todos los campos.');
    }
  });
  
  